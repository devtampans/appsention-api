--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: absensi; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.absensi (
    user_id integer NOT NULL,
    company_id integer NOT NULL,
    building_id integer NOT NULL,
    note text,
    "time" date NOT NULL,
    status character varying(10)
);


ALTER TABLE public.absensi OWNER TO deritarigan;

--
-- Name: building; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.building (
    company_id smallint NOT NULL,
    building_id integer NOT NULL,
    building_name character varying(255) NOT NULL
);


ALTER TABLE public.building OWNER TO deritarigan;

--
-- Name: building_building_id_seq; Type: SEQUENCE; Schema: public; Owner: deritarigan
--

CREATE SEQUENCE public.building_building_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.building_building_id_seq OWNER TO deritarigan;

--
-- Name: building_building_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: deritarigan
--

ALTER SEQUENCE public.building_building_id_seq OWNED BY public.building.building_id;


--
-- Name: company; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.company (
    company_id integer NOT NULL,
    company_name character varying NOT NULL,
    address character varying,
    city character varying,
    state_province character varying
);


ALTER TABLE public.company OWNER TO deritarigan;

--
-- Name: company_company_id_seq; Type: SEQUENCE; Schema: public; Owner: deritarigan
--

CREATE SEQUENCE public.company_company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_company_id_seq OWNER TO deritarigan;

--
-- Name: company_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: deritarigan
--

ALTER SEQUENCE public.company_company_id_seq OWNED BY public.company.company_id;


--
-- Name: job; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.job (
    job_id integer NOT NULL,
    job_name character varying(100) NOT NULL,
    max_salary numeric(15,2),
    min_salary numeric(15,2)
);


ALTER TABLE public.job OWNER TO deritarigan;

--
-- Name: job_job_id_seq; Type: SEQUENCE; Schema: public; Owner: deritarigan
--

CREATE SEQUENCE public.job_job_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.job_job_id_seq OWNER TO deritarigan;

--
-- Name: job_job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: deritarigan
--

ALTER SEQUENCE public.job_job_id_seq OWNED BY public.job.job_id;


--
-- Name: latlong; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.latlong (
    building_id smallint NOT NULL,
    latitude numeric(11,7) NOT NULL,
    longitude numeric(11,7) NOT NULL
);


ALTER TABLE public.latlong OWNER TO deritarigan;

--
-- Name: position; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public."position" (
    user_id integer NOT NULL,
    latitude numeric(11,7) NOT NULL,
    longitude numeric(11,7) NOT NULL,
    duration integer,
    listener boolean
);


ALTER TABLE public."position" OWNER TO deritarigan;

--
-- Name: profile; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.profile (
    user_id integer,
    first_name character varying(25) NOT NULL,
    last_name character varying(25) NOT NULL,
    email character varying,
    phone character varying(15),
    hire_date date,
    job_id integer,
    company_id integer,
    role character varying(20)
);


ALTER TABLE public.profile OWNER TO deritarigan;

--
-- Name: users; Type: TABLE; Schema: public; Owner: deritarigan
--

CREATE TABLE public.users (
    user_id integer NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    token text NOT NULL
);


ALTER TABLE public.users OWNER TO deritarigan;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: deritarigan
--

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO deritarigan;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: deritarigan
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;


--
-- Name: building building_id; Type: DEFAULT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.building ALTER COLUMN building_id SET DEFAULT nextval('public.building_building_id_seq'::regclass);


--
-- Name: company company_id; Type: DEFAULT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.company ALTER COLUMN company_id SET DEFAULT nextval('public.company_company_id_seq'::regclass);


--
-- Name: job job_id; Type: DEFAULT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.job ALTER COLUMN job_id SET DEFAULT nextval('public.job_job_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Data for Name: absensi; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.absensi (user_id, company_id, building_id, note, "time", status) FROM stdin;
12	2	10	0	2019-12-08	clock-in
12	2	10	0	2019-12-08	clock-in
12	2	10	mantap	2019-12-08	clock-in
12	2	10	mantap	2019-08-20	clock-in
12	2	10	mantap	2019-08-20	clock-in
12	2	10	mantap	2019-08-20	clock-in
12	2	10	mantap	2019-08-20	clock-in
12	2	10	mantap	2019-08-20	clock-in
12	2	10	mantap	2019-08-20	clock-in
12	2	11	Note :	2019-02-17	Clock-in
12	2	11	Note :	2019-02-17	Clock-in
12	2	11	wadawwww	2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-in
12	2	11	gagoo	2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-out
12	2	10	mantap	2019-08-20	clock-in
8	1	9	mantap	2019-08-20	clock-in
8	1	9	mantap	2019-08-20	clock-in
12	2	11		2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-in
12	2	11		2019-02-17	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-18	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
12	2	11		2019-02-19	Clock-in
8	1	9	mantap	2019-08-20	clock-in
12	1	9	mantap	2019-08-20	clock-in
12	1	9	mantap	2019-08-20	clock-in
12	1	9	mantap	2019-08-20	clock-in
12	2	11		2019-02-20	Clock-in
12	2	11		2019-02-20	Clock-out
12	2	11		2019-02-20	Clock-in
12	2	11		2019-02-20	Clock-in
12	2	11		2019-02-20	Clock-out
13	2	11		2019-02-20	Clock-in
13	2	11		2019-02-20	Clock-in
13	2	11		2019-02-20	Clock-in
13	2	11		2019-02-20	Clock-in
13	2	11		2019-02-20	Clock-in
13	2	11		2019-02-20	Clock-in
13	2	11		2019-02-20	Clock-in
14	2	11		2019-02-20	Clock-in
14	2	11		2019-02-20	Clock-out
14	2	11		2019-02-20	Clock-in
13	2	11		2019-02-21	Clock-in
13	2	11		2019-02-21	Clock-in
13	2	11		2019-02-21	Clock-out
\.


--
-- Data for Name: building; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.building (company_id, building_id, building_name) FROM stdin;
1	1	President University
1	2	Kantin President University
2	3	Gedung Mantap
3	4	Gedung Kurang Mantap
2	5	Gedung Coba
2	6	Gedung Coba2
2	7	Gedung Coba2
2	8	Gedung Coba3
1	9	governement office
2	10	KPP Cikarang
2	11	KPP Cikarang
2	12	toko
2	13	toko
\.


--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.company (company_id, company_name, address, city, state_province) FROM stdin;
1	PT.Coba	Jl.Kenangan	Medan	North Sumatra
2	PT.Karya	Jl.Kasih	Medan	North Sumatra
3	PT.Adil	Jl.Jujur	Medan	North Sumatra
\.


--
-- Data for Name: job; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.job (job_id, job_name, max_salary, min_salary) FROM stdin;
1	Mobile Developer	10000000.00	8000000.00
2	Web Developer	10000000.00	8000000.00
3	Back End Developer	10000000.00	8000000.00
\.


--
-- Data for Name: latlong; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.latlong (building_id, latitude, longitude) FROM stdin;
1	-6.2845030	107.1703340
1	-6.2844520	107.1703770
1	-6.2844080	107.1703150
1	-6.2844610	107.1702720
2	-6.2848710	107.1702790
2	-6.2846690	107.1704440
2	-6.2849650	107.1708700
2	-6.2852080	107.1706780
3	-12.1232130	107.2323230
3	-12.1232120	107.2123232
4	-12.1123131	107.3332323
5	1.0000000	2.0000000
5	3.0000000	4.0000000
5	5.0000000	6.0000000
6	2.0000000	3.0000000
6	4.0000000	5.0000000
6	6.0000000	7.0000000
7	2.0000000	3.0000000
7	4.0000000	5.0000000
7	6.0000000	7.0000000
8	2.0000000	3.0000000
8	4.0000000	5.0000000
8	6.0000000	7.0000000
9	-6.2853900	107.1713250
9	-6.2856330	107.1711350
9	-6.2856140	107.1711130
9	-6.2856650	107.1710690
9	-6.2854840	107.1708420
9	-6.2854270	107.1708860
9	-6.2854210	107.1708790
9	-6.2851860	107.1710720
9	-6.2852310	107.1711090
9	-6.2851660	107.1711410
9	-6.2853030	107.1713170
9	-6.2853360	107.1712610
10	-6.2853900	107.1713250
10	-6.2856330	107.1711350
10	-6.2856140	107.1711130
10	-6.2856650	107.1710690
10	-6.2854840	107.1708420
10	-6.2854270	107.1708860
10	-6.2854210	107.1708790
10	-6.2851860	107.1710720
10	-6.2852310	107.1711090
10	-6.2851660	107.1711410
10	-6.2853030	107.1713170
10	-6.2853360	107.1712610
11	-6.2831520	107.1672830
11	-6.2829810	107.1673390
11	-6.2830220	107.1674550
11	-6.2829310	107.1674860
11	-6.2829500	107.1675400
11	-6.2832030	107.1674540
12	-6.2830717	107.1674139
12	-6.2830717	107.1674139
12	-6.2830717	107.1674139
13	-6.2830830	107.1674216
13	-6.2830830	107.1674216
13	-6.2830830	107.1674216
\.


--
-- Data for Name: position; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public."position" (user_id, latitude, longitude, duration, listener) FROM stdin;
8	0.2000000	0.1000000	\N	t
123	0.0000000	0.0000000	\N	\N
13	-6.2830830	107.1674208	6000	f
14	-6.2830994	107.1674100	6000	t
12	-6.2831415	107.1674159	5000	f
15	0.5000000	0.5000000	6000	\N
\.


--
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.profile (user_id, first_name, last_name, email, phone, hire_date, job_id, company_id, role) FROM stdin;
1	Deri	Tarigan	deritarigan@gmail.com	087708180002	\N	\N	\N	\N
6	Deri	Tarigan	deritarigan@gmail.com	087708180002	\N	2	1	\N
8	Deri	Tarigan	deritarigan@gmail.com	087708180002	\N	2	1	\N
10	Deri	Tarigan	deritarigan@gmail.com	087708180002	\N	2	1	\N
12	mirna	nainggolan	mirnanainggolan@gmail.com	087708180000	\N	3	2	\N
13	tari	gan	mirnanainggolan@gmail.com	087708180000	\N	3	2	\N
14	pinem	mantap	mirnanainggolan@gmail.com	087708180000	\N	3	2	\N
15	pinem	mantap	mirnanainggolan@gmail.com	087708180000	\N	3	2	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: deritarigan
--

COPY public.users (user_id, username, password, token) FROM stdin;
2	bagong	enak	token1
6	tampan	$2a$10$qUljVMlenICyygoguMF56eelYVtXs44Km7zCp3puTJtahp6ExEo.i	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDk0MzAwMTY3MjAsInVzZXJuYW1lIjoidGFtcGFuIn0.jjra3wRVjCoL1LqawpVrqvSZjyk7YH72r-h-NDTXwCU
8	cakepbanget	$2a$10$W.1EsrvJSdQ3AF4CqZs8OOoPSZkB89bxBTUv0TdddfUIndJabun3S	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNDA4NjUwNTY5IiwidXNlcm5hbWUiOiJjYWtlcGJhbmdldCJ9.FVhl76bxLePXuXRQOHr3inWRxWLZ_avB9KHAsQ3Y1Jg
4	ganteng	$2a$10$iOxNRKZDq.zHaYk2T9SE2.rb5s.VN9W312V0cGAt7oFO4r.072tra	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNjA1NDg5MzQ5IiwidXNlcm5hbWUiOiJnYW50ZW5nIn0.SpgTJas18zMf5JjeVOwoIsgMJGZczPmyeQiR0F-Zm_I
10	akuganteng	$2a$10$7U4viZnCasIYtzcGF/O1x.ftaBjBtrKDvtXJm0HOaF9BFXDPAjmdu	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNjA1NjE5Njg2IiwidXNlcm5hbWUiOiJha3VnYW50ZW5nIn0.XkHC2IBeDt4Z_ZSfkXvQdXvf8f6ZA0nmJmiINH6HxRg
7	cakep	$2a$10$CzZkwijzxV0S7AHTiKcPCOkPz8yh3AnlHycNa0ETV0G5AeytLccMK	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNjA1Njg4Mjc0IiwidXNlcm5hbWUiOiJjYWtlcCJ9.hAHHnag3x5xDxq3z3IUH_NvMjKO-dxadY58wsIrnN5c
12	mirna	$2a$10$m9GO2NWaLtywKwUdQCoVDebIBssZdOlZhKjfFSF1v5Tg2mx9hakHu	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNjA2Mjc1ODA0IiwidXNlcm5hbWUiOiJtaXJuYSJ9.GjGnp5FWD_KeW8CYFkJjNJ234ti1n-ynP-4_rIReTLM
13	tarigan	$2a$10$3Ia3NL4WZOlLSvIFObwiaehWbfLxvQ7IlyBN7leWarH8ZlNrrN.ym	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNjQ5ODkzMDgyIiwidXNlcm5hbWUiOiJ0YXJpZ2FuIn0.Oo9oSBLt_EDN8_n8uMhPUX1kdj_MYv-uFAHBPVsBqzg
15	jahat	$2a$10$JIEu/NbXULSJvH5su3QgCO2A6Q3ERq/eBqUc88me2VSqc2VvsRIEy	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNjg0Njc0NDUxIiwidXNlcm5hbWUiOiJqYWhhdCJ9.lQgpHSG0GaOmAAY6maQxYaNMt5AxDtP8vTxxB0X5thg
14	pinem	$2a$10$8693WBFx/Dq25x1byAZpX.p.OyENGZSI8vBkE6K9hcKRk.ZKsgvOa	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxNTUwNzI4Njc4MTAxIiwidXNlcm5hbWUiOiJwaW5lbSJ9.qYiEHmm3RL9euZZnI-vFSTjE84ryLsJxVrJfx89u2Mk
\.


--
-- Name: building_building_id_seq; Type: SEQUENCE SET; Schema: public; Owner: deritarigan
--

SELECT pg_catalog.setval('public.building_building_id_seq', 13, true);


--
-- Name: company_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: deritarigan
--

SELECT pg_catalog.setval('public.company_company_id_seq', 3, true);


--
-- Name: job_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: deritarigan
--

SELECT pg_catalog.setval('public.job_job_id_seq', 3, true);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: deritarigan
--

SELECT pg_catalog.setval('public.users_user_id_seq', 15, true);


--
-- Name: building building_pkey; Type: CONSTRAINT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.building
    ADD CONSTRAINT building_pkey PRIMARY KEY (building_id);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (company_id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (job_id);


--
-- Name: position position_pkey; Type: CONSTRAINT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public."position"
    ADD CONSTRAINT position_pkey PRIMARY KEY (user_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: deritarigan
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- PostgreSQL database dump complete
--

