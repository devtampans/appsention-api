package main

import (
	"context"
	"controllers"
	"database/sql"
	"driver"
	"fmt"
	"log"
	"models"
	"net/http"

	// "os"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	//"github.com/davecgh/go-spew/spew"
)

const (
	host     = "localhost"
	port     = 5432
	users    = "deritarigan"
	password = "deri123"
	dbname   = "login"
)

var user []models.User
var ctx = context.Background()
var psqlInfo string
var db *sql.DB
var err error

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {

	db = driver.ConnectDB()
	controller := controllers.Controllers{}
	// port := os.Getenv("PORT")

	router := mux.NewRouter()
	router.HandleFunc("/user/{id}", controller.GetUser(db)).Methods("GET")
	router.HandleFunc("/auth/login", controller.Login(db)).Methods("POST")
	router.HandleFunc("/user", controller.AddUser(db)).Methods("POST")
	router.HandleFunc("/users", controller.GetAllUser(db)).Methods("GET")
	router.HandleFunc("/update-profile", controller.UpdateProfile(db)).Methods("POST")
	router.HandleFunc("/user/change-password", controller.ChangeUserPassword(db)).Methods("POST")
	router.HandleFunc("/auth/sign-up", controller.SignUp(db)).Methods("POST")
	router.HandleFunc("/company", controller.GetCompanies(db)).Methods("GET")
	router.HandleFunc("/job", controller.GetJob(db)).Methods("GET")
	router.HandleFunc("/building/{company_id}", controller.GetBuilding(db)).Methods("GET")
	router.HandleFunc("/add-building", controller.AddBuilding(db)).Methods("POST")
	router.HandleFunc("/clock-in", controller.ClockIn(db)).Methods("POST")
	router.HandleFunc("/update-position", controller.UpdatePosition(db)).Methods("POST")
	router.HandleFunc("/employee-position", controller.GetEmployeePosition(db)).Methods("POST")

	var imageserver = http.FileServer(http.Dir("./assets/"))
	router.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageserver))
	// router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("assets"))))
	// http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("assets"))))

	fmt.Println("Successfully connected!")
	staticPort := "38585"
	log.Fatal(http.ListenAndServe(":"+staticPort, router))

}
