package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"models"
	"net/http"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

//Controllers to initiate controllers
type Controllers struct{}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
func setControllerDB(database *sql.DB) {
	db = database
}

func respondWithError(w http.ResponseWriter, status int, error models.ErrorModel) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	var emptyData models.EmptyData
	emptyData.Meta = error
	responseJSON(w, emptyData)
}

func responseJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}

func checkPassword(db *sql.DB, passwordModel models.ChangePassword) bool {
	var currPassword string
	var status = false

	err = db.QueryRow("select password from users where user_id =$1 AND password=$2",
		passwordModel.ID, passwordModel.Password).Scan(&currPassword)
	logFatal(err)
	if currPassword != "" {
		status = true
	}
	return status
}

//GenerateToken is function to generate JWT
func GenerateToken(user string) (string, error) {
	var err error
	secret := "secret"

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user,
		"iss":      strconv.FormatInt(GetCurrentTimeMillis(), 10),
	})
	tokenString, err := token.SignedString([]byte(secret))

	return tokenString, err
}

//GetCurrentTimeMillis to generate time in epoch
func GetCurrentTimeMillis() int64 {
	nanos := time.Now().UnixNano()

	return nanos / 1000000
}

//CompareInsensitive is a function to comparing string value
func CompareInsensitive(a, b string) bool {
	// a quick optimization. If the two strings have a different
	// length then they certainly are not the same
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		// if the characters already match then we don't need to
		// alter their case. We can continue to the next rune
		if a[i] == b[i] {
			continue
		} else {
			return false
		}
	}
	// The string length has been traversed without a mismatch
	// therefore the two match
	return true
}

func verifyToken(r *http.Request) (string, jwt.MapClaims) {
	authHeader := r.Header.Get("Authorization")
	bearerToken := strings.Split(authHeader, " ")
	if len(bearerToken) == 2 {
		authToken := bearerToken[1]

		token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error")
			}
			return []byte("secret"), nil
		})
		if err != nil {
			return "", nil
		}
		logFatal(err)
		if token.Valid {
			jwtMap := token.Claims.(jwt.MapClaims)
			return authToken, jwtMap
		}
	}
	return "", nil
}
