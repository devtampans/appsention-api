package controllers

import (
	"database/sql"
	"encoding/json"
	"models"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//GetBuilding to get all Latlong of Buildings then signed in the company
func (c Controllers) GetBuilding(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var addBuilding models.BuildingDatabaseModel
		var addBuildings []models.BuildingDatabaseModel
		var BuildingResponse models.BuildingResponse
		var Buildings []models.Building
		var BuildingData models.BuildingData
		var Building models.Building
		var Latlongs []models.Latlong
		var Latlong models.Latlong
		var errorModel models.ErrorModel
		var CurrentID = 0

		params := mux.Vars(r)
		id, _ := strconv.Atoi(params["company_id"])

		rows, err := db.Query("select building.building_id,building.building_name"+
			",latlong.latitude,latlong.longitude from company inner join building on "+
			"(company.company_id = building.company_id) inner join latlong on (building"+
			".building_id = latlong.building_id) where company.company_id = $1;", id)
		logFatal(err)

		defer rows.Close()
		i := 0

		for rows.Next() {
			rows.Scan(&addBuilding.BuildingID, &addBuilding.BuildingName, &addBuilding.Latitude,
				&addBuilding.Longitude)
			logFatal(err)
			if i == 0 {
				CurrentID = addBuilding.BuildingID
				Building.BuildingName = addBuilding.BuildingName
				Building.BuildingID = addBuilding.BuildingID
			}

			if CurrentID != addBuilding.BuildingID {
				Building.Latlongs = Latlongs
				Buildings = append(Buildings, Building)
				Building.BuildingID = addBuilding.BuildingID
				Building.BuildingName = addBuilding.BuildingName
				Latlongs = []models.Latlong{}
				CurrentID = addBuilding.BuildingID
			}
			Latlong.Latitude = addBuilding.Latitude
			Latlong.Longitude = addBuilding.Longitude

			Latlongs = append(Latlongs, Latlong)
			i = i + 1
			addBuildings = append(addBuildings, addBuilding)
		}
		Building.Latlongs = Latlongs
		Buildings = append(Buildings, Building)
		Latlongs = Latlongs[:0]
		CurrentID = addBuilding.BuildingID

		BuildingData.Buildings = Buildings
		errorModel.Message = "Success!"
		errorModel.Code = 200
		errorModel.Status = true
		BuildingResponse.DataBean = BuildingData
		BuildingResponse.MetaBean = errorModel
		// rows, err := db.QueryRowContext(ctx, "select * from users;")
		responseJSON(w, BuildingResponse)

	}
}

//AddBuilding is endpoint to add new building with latitude and longitude
func (c Controllers) AddBuilding(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var AddBuilding models.AddBuildingRequest
		var AddBuildingResponse models.AddBuildingResponse
		var BuildingID int
		var errorModel models.ErrorModel
		var currentJWT string

		authToken, token := verifyToken(r)

		if token == nil {
			errorModel.Message = err.Error()
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)
			return
		}

		username := token["username"]

		err = db.QueryRow("select token from users where username = $1", username).Scan(&currentJWT)

		if !CompareInsensitive(currentJWT, authToken) {
			errorModel.Message = "Unauthorized"
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)

			return
		}

		json.NewDecoder(r.Body).Decode(&AddBuilding)

		err = db.QueryRow("insert into building (company_id,building_name) values ($1,$2) RETURNING building_id;",
			AddBuilding.CompanyID, AddBuilding.BuildingName).Scan(&BuildingID)
		logFatal(err)

		for i := 0; i < len(AddBuilding.Latlong); i++ {
			rows, err := db.Query("insert into latlong (building_id,latitude,longitude) values ($1,$2,$3);", BuildingID, AddBuilding.Latlong[i].Latitude, AddBuilding.Latlong[i].Longitude)
			logFatal(err)
			defer rows.Close()
		}

		AddBuildingResponse.DataBean.BuildingID = BuildingID
		AddBuildingResponse.DataBean.BuildingName = AddBuilding.BuildingName
		AddBuildingResponse.DataBean.Latlong = AddBuilding.Latlong

		errorModel.Message = "Success!"
		errorModel.Code = 200
		errorModel.Status = true

		AddBuildingResponse.MetaBean = errorModel

		responseJSON(w, AddBuildingResponse)
	}
}
