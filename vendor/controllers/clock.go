package controllers

import (
	"database/sql"
	"encoding/json"
	"models"
	"net/http"
)

//ClockIn is endpoint to perform clock in request
func (c Controllers) ClockIn(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ClockingRequest models.ClockingRequest
		var ClockingResponse models.UpdatePositionResponse
		var errorModel models.ErrorModel
		var currentJWT string
		var Listener bool
		var UpdateDuration int

		authToken, token := verifyToken(r)

		if token == nil {
			errorModel.Message = err.Error()
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)
			return
		}

		username := token["username"]

		err = db.QueryRow("select token from users where username = $1", username).Scan(&currentJWT)

		if !CompareInsensitive(currentJWT, authToken) {
			errorModel.Message = "Unauthorized"
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)

			return
		}

		json.NewDecoder(r.Body).Decode(&ClockingRequest)

		rows, err := db.Query("insert into absensi (user_id,company_id,building_id,note,time,status) values ($1,$2,$3,$4,$5,$6);",
			ClockingRequest.UserID, ClockingRequest.CompanyID, ClockingRequest.BuildingID,
			ClockingRequest.Note, ClockingRequest.Date+" "+ClockingRequest.Time, ClockingRequest.Status)
		logFatal(err)
		defer rows.Close()

		rows, err = db.Query("update position set latitude =$1 , longitude =$2 , listener = $3 where user_id =$4 returning duration,listener;",
			ClockingRequest.Location.Latitude, ClockingRequest.Location.Longitude,
			ClockingRequest.Listener, ClockingRequest.UserID)
		defer rows.Close()

		if !rows.Next() {
			row, err := db.Query("insert into position (user_id,latitude,longitude,duration,listener) values ($1,$2,$3,6000,true) returning duration,listener;",
				ClockingRequest.UserID, ClockingRequest.Location.Latitude, ClockingRequest.Location.Longitude)
			logFatal(err)
			defer row.Close()
			row.Scan(&UpdateDuration, &Listener)
		} else {
			rows.Scan(&UpdateDuration, &Listener)
		}

		// AddBuildingResponse.DataBean.BuildingID = BuildingID
		// AddBuildingResponse.DataBean.BuildingName = AddBuilding.BuildingName
		// AddBuildingResponse.DataBean.Latlong = AddBuilding.Latlong

		ClockingResponse.DataUpdate.Message = "Clocking success"
		ClockingResponse.DataUpdate.TimeInMillis = UpdateDuration
		ClockingResponse.DataUpdate.Listener = Listener

		errorModel.Message = "Success!"
		errorModel.Code = 200
		errorModel.Status = true

		ClockingResponse.ErrorModel = errorModel

		// AddBuildingResponse.MetaBean = errorModel

		responseJSON(w, ClockingResponse)
	}
}

//UpdatePosition is endpoint to perform update position by user
func (c Controllers) UpdatePosition(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var UpdateRequest models.UpdatePositionRequest
		var UpdatePositionResponse models.UpdatePositionResponse
		var errorModel models.ErrorModel
		var currentJWT string
		var UpdateTime int
		var Listener bool

		authToken, token := verifyToken(r)

		if token == nil {
			errorModel.Message = err.Error()
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)
			return
		}

		username := token["username"]

		err = db.QueryRow("select token from users where username = $1", username).Scan(&currentJWT)

		if !CompareInsensitive(currentJWT, authToken) {
			errorModel.Message = "Unauthorized"
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)

			return
		}

		json.NewDecoder(r.Body).Decode(&UpdateRequest)

		err = db.QueryRow("update position set latitude =$1 , longitude =$2 where user_id =$3 returning duration,listener;",
			UpdateRequest.Location.Latitude, UpdateRequest.Location.Longitude,
			UpdateRequest.UserID).Scan(&UpdateTime, &Listener)
		logFatal(err)

		UpdatePositionResponse.DataUpdate.Message = "Update position success"
		UpdatePositionResponse.DataUpdate.TimeInMillis = UpdateTime
		UpdatePositionResponse.DataUpdate.Listener = Listener

		// AddBuildingResponse.DataBean.BuildingID = BuildingID
		// AddBuildingResponse.DataBean.BuildingName = AddBuilding.BuildingName
		// AddBuildingResponse.DataBean.Latlong = AddBuilding.Latlong

		errorModel.Message = "Success!"
		errorModel.Code = 200
		errorModel.Status = true
		UpdatePositionResponse.ErrorModel = errorModel

		// AddBuildingResponse.MetaBean = errorModel

		responseJSON(w, UpdatePositionResponse)
	}
}

//GetEmployeePosition is the endpoint to perform get employee
func (c Controllers) GetEmployeePosition(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var errorModel models.ErrorModel
		var currentJWT string
		var GetPositionRequest models.GetPositionRequest
		var GetPositionResponse models.GetPositionResponse
		var EmployeeData models.EmployeeData
		var Employees []models.EmployeeData

		authToken, token := verifyToken(r)

		if token == nil {
			errorModel.Message = err.Error()
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)
			return
		}

		username := token["username"]

		err = db.QueryRow("select token from users where username = $1", username).Scan(&currentJWT)

		if !CompareInsensitive(currentJWT, authToken) {
			errorModel.Message = "Unauthorized"
			errorModel.Code = 401
			errorModel.Status = false
			respondWithError(w, http.StatusUnauthorized, errorModel)

			return
		}

		json.NewDecoder(r.Body).Decode(&GetPositionRequest)

		rows, err := db.Query("select position.user_id,profile.first_name,profile.last_name,company.company_id,"+
			"company.company_name, position.latitude, position.longitude from profile inner join position"+
			" on (profile.user_id = position.user_id) inner join company on (profile.company_id = company.company_id)"+
			"where company.company_id = $1;", GetPositionRequest.CompanyID)
		logFatal(err)
		defer rows.Close()

		for rows.Next() {
			rows.Scan(&EmployeeData.UserID, &EmployeeData.FirstName, &EmployeeData.LastName,
				&GetPositionResponse.DataPosition.CompanyID, &GetPositionResponse.DataPosition.CompanyName,
				&EmployeeData.Position.Latitude, &EmployeeData.Position.Longitude)
			Employees = append(Employees, EmployeeData)
		}
		errorModel.Message = "Success!"
		errorModel.Code = 200
		errorModel.Status = true

		GetPositionResponse.DataPosition.Employee = Employees
		GetPositionResponse.ErrorModel = errorModel

		responseJSON(w, GetPositionResponse)

	}
}
