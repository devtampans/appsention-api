package controllers

import (
	"bytes"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"image/jpeg"
	"models"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

var user []models.User
var db *sql.DB
var err error

//GetAllUser to get all user
func (c Controllers) GetAllUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var addUser models.User
		user = []models.User{}

		rows, err := db.Query("select user_id,username,password from users")
		logFatal(err)

		defer rows.Close()

		for rows.Next() {
			rows.Scan(&addUser.ID, &addUser.Username, &addUser.Password)
			logFatal(err)

			user = append(user, addUser)

		}

		// rows, err := db.QueryRowContext(ctx, "select * from users;")
		responseJSON(w, user)

	}
}

//UpdateProfile is a function to handle update profile.
func (c Controllers) UpdateProfile(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ProfileResponse models.ProfileResponse
		var updateProfile models.DataProfileUser
		var errModel models.ErrorModel

		json.NewDecoder(r.Body).Decode(&updateProfile)

		updateProfile.Image = cnvrtStrBase64toImg(updateProfile.Image, updateProfile.ID)

		err = db.QueryRow("update profile set first_name =$1, last_name =$2, email = $3 , phone = $4 , image_url = $5"+
			"where user_id =$6 returning first_name,last_name,email,phone;", updateProfile.FirstName,
			updateProfile.LastName, updateProfile.Email, updateProfile.Phone, updateProfile.Image, updateProfile.ID).
			Scan(&updateProfile.FirstName, &updateProfile.LastName, &updateProfile.Email,
				&updateProfile.Phone)

		errModel.Code = 200
		errModel.Message = "Success!!"
		errModel.Status = true

		ProfileResponse.Data = updateProfile
		ProfileResponse.Meta = errModel
		responseJSON(w, ProfileResponse)
	}
}

func cnvrtStrBase64toImg(strBase string, ID int) string {
	var f *os.File
	var imagePath = strconv.Itoa(ID) + strconv.FormatInt(GetCurrentTimeMillis(), 8) + ".jpg"

	coI := strings.Index(string(strBase), ",")
	rawImage := string(strBase)[coI+1:]
	if rawImage != "" {
		var decodeByte, _ = base64.StdEncoding.DecodeString(string(rawImage))

		res := bytes.NewReader(decodeByte)
		path, _ := os.Getwd()

		jpgI, errJpg := jpeg.Decode(res)
		if errJpg == nil {
			var folder = path + "/assets/" + strconv.Itoa(ID)
			if _, err := os.Stat(folder); os.IsNotExist(err) {
				err = os.MkdirAll(folder, 0777)
				if err != nil {
					panic(err)
				}
			}

			f, _ = os.OpenFile(path+"/assets/"+strconv.Itoa(ID)+"/"+imagePath, os.O_WRONLY|os.O_CREATE, 0777)
			jpeg.Encode(f, jpgI, &jpeg.Options{Quality: 75})
			fmt.Println("Jpg created")
		} else {
			logFatal(errJpg)
		}

		defer func() {
			f.Close()
		}()
	}

	return "images/" + strconv.Itoa(ID) + "/" + imagePath
}

//ChangeUserPassword to change password
func (c Controllers) ChangeUserPassword(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var changePassword models.ChangePassword
		// var users models.User

		json.NewDecoder(r.Body).Decode(&changePassword)
		// if checkPassword(db, changePassword) {
		// 	if changePassword.NewPassword != "" {
		// 		if changePassword.NewPassword == changePassword.ConfirmPassword {
		// 			err = db.QueryRow(
		// 				"update users set password =$1 where user_id =$2 returning user_id,username,password;",
		// 				changePassword.NewPassword, changePassword.ID,
		// 			).Scan(&users.ID, &users.Username, &users.Password)
		// 			logFatal(err)
		// 		}
		// 	} else {
		// 		responseJSON(w, changePassword)
		// 	}
		// } else {
		// 	logFatal(err)
		// }
		// responseJSON(w, users)

		responseJSON(w, changePassword)

	}
}

//GetUser to get user as id
func (c Controllers) GetUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ProfileResponse models.ProfileResponse
		var errorModel models.ErrorModel
		var dataProfile models.DataProfileUser

		params := mux.Vars(r)
		id, _ := strconv.Atoi(params["id"])
		dataProfile.ID = id
		err = db.QueryRow("select first_name, last_name,email,phone,image_url from profile WHERE user_id=$1", id).Scan(
			&dataProfile.FirstName, &dataProfile.LastName, &dataProfile.Email, &dataProfile.Phone, &dataProfile.Image)
		if err != nil {
			if err == sql.ErrNoRows {
				errorModel.Message = "User doesnt exist"
				errorModel.Code = 404
				errorModel.Status = false
				respondWithError(w, http.StatusBadRequest, errorModel)
				return
			}
			logFatal(err)
		}
		errorModel.Message = "Success!!"
		errorModel.Status = true

		ProfileResponse.Data = dataProfile
		ProfileResponse.Meta = errorModel

		responseJSON(w, ProfileResponse)

	}
}

//Login as endpoint to call authentication login
func (c Controllers) Login(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User
		var errorModel models.ErrorModel
		var LoginResponseModel models.LoginResponse
		var DataUser models.DataUser

		json.NewDecoder(r.Body).Decode(&user)

		if user.Username == "" {
			errorModel.Message = "Username is missing"
			respondWithError(w, http.StatusBadRequest, errorModel)
			return
		}
		if user.Password == "" {
			errorModel.Message = "Password is missing"
			respondWithError(w, http.StatusBadRequest, errorModel)
			return
		}

		userPassword := user.Password

		row := db.QueryRow("select * from users where username =$1", user.Username)
		err := row.Scan(&user.ID, &user.Username, &user.Password, &user.Token)

		if err != nil {
			if err == sql.ErrNoRows {
				errorModel.Message = "User doesnt exist"
				errorModel.Code = 404
				errorModel.Status = false
				respondWithError(w, http.StatusBadRequest, errorModel)
				return
			}
			logFatal(err)
		}
		hashedPassword := user.Password

		err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(userPassword))
		if err != nil {
			errorModel.Message = "Invalid password"
			respondWithError(w, http.StatusBadRequest, errorModel)
			return
		}

		token, err := GenerateToken(user.Username)
		logFatal(err)

		err = db.QueryRow("update users set token =$1 where user_id =$2 returning token,user_id;", &token,
			user.ID).Scan(&DataUser.Token, &DataUser.ID)
		logFatal(err)

		// err = db.QueryRow("select first_name,last_name,email,phone,job_id,company_id from profile where user_id = $1",
		// 	&DataUser.ID).Scan(&DataUser.FirstName, &DataUser.LastName, &DataUser.Email, &DataUser.Phone, &DataUser.JobID, &DataUser.CompanyID)
		err = db.QueryRow("select profile.first_name,profile.last_name,profile.email,profile.phone,job.job_name,profile.company_id,profile.image_url from profile inner join job on (profile.job_id = job.job_id) where user_id = $1",
			&DataUser.ID).Scan(&DataUser.FirstName, &DataUser.LastName, &DataUser.Email, &DataUser.Phone, &DataUser.JobName, &DataUser.CompanyID, &DataUser.Image)

		errorModel.Code = 200
		errorModel.Message = "Success!!"
		errorModel.Status = true

		LoginResponseModel.Data = DataUser
		LoginResponseModel.Meta = errorModel

		responseJSON(w, LoginResponseModel)
	}
}

//GetCompanies is the function of an endpoint to get all company data
func (c Controllers) GetCompanies(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var CompanyResponseModel models.CompanyResponse
		var DataBean models.DataCompanies
		var companies []models.Company
		var company models.Company
		var meta models.ErrorModel

		rows, err := db.Query("select company_id,company_name from company")

		defer rows.Close()

		if err != nil {
			meta.Message = "Internal server error!"
			meta.Code = 500
			meta.Status = false
			respondWithError(w, http.StatusInternalServerError, meta)
			logFatal(err)
			return
		}

		for rows.Next() {
			rows.Scan(&company.ID, &company.CompanyName)
			logFatal(err)

			companies = append(companies, company)

		}
		meta.Message = "Success!"
		meta.Code = 200
		meta.Status = true

		DataBean.Companies = companies
		CompanyResponseModel.Data = DataBean
		CompanyResponseModel.Meta = meta

		// rows, err := db.QueryRowContext(ctx, "select * from users;")
		responseJSON(w, CompanyResponseModel)

	}
}

//GetJob is a function of endpoint to get job detail
func (c Controllers) GetJob(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var JobResponseModel models.JobResponse
		var DataBean models.DataJob
		var jobs []models.JobModel
		var job models.JobModel
		var meta models.ErrorModel

		rows, err := db.Query("select job_id,job_name from job")

		defer rows.Close()

		if err != nil {
			meta.Message = "Internal server error!"
			meta.Code = 500
			meta.Status = false
			respondWithError(w, http.StatusInternalServerError, meta)
			logFatal(err)
			return
		}

		for rows.Next() {
			rows.Scan(&job.ID, &job.JobName)
			logFatal(err)

			jobs = append(jobs, job)

		}
		meta.Message = "Success!"
		meta.Code = 200
		meta.Status = true

		DataBean.Jobs = jobs
		JobResponseModel.Data = DataBean
		JobResponseModel.Meta = meta

		// rows, err := db.QueryRowContext(ctx, "select * from users;")
		responseJSON(w, JobResponseModel)

	}
}

//SignUp is a function to let user register an account
func (c Controllers) SignUp(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// var user model.user
		var SignUpRequest models.SignUpRequest
		var errorModel models.ErrorModel
		var DataBean models.DataSignup
		var SignUpResponse models.SignUpResponse

		json.NewDecoder(r.Body).Decode(&SignUpRequest)

		if SignUpRequest.Username == "" {
			errorModel.Message = "Email is missing"
			errorModel.Code = 400
			errorModel.Status = false
			respondWithError(w, 200, errorModel)
			return
		}
		if SignUpRequest.Password == "" {
			errorModel.Message = "Password is missing"
			errorModel.Code = 400
			errorModel.Status = false
			respondWithError(w, 200, errorModel)
			return
		}

		hash, err := bcrypt.GenerateFromPassword([]byte(SignUpRequest.Password), 10)
		SignUpRequest.Password = string(hash)

		token, err := GenerateToken(SignUpRequest.Username)
		logFatal(err)

		err = db.QueryRow("insert into users (username , password, token) values ($1,$2,$3) RETURNING user_id,token;",
			SignUpRequest.Username, SignUpRequest.Password, token).Scan(&DataBean.UserID, &DataBean.AuthToken)

		if err != nil {
			pqErr := err.(*pq.Error)
			if pqErr.Code == "23505" {
				errorModel.Message = "Username already exist"
				errorModel.Code = 400
				errorModel.Status = false
				respondWithError(w, http.StatusBadRequest, errorModel)
				return
			}
			errorModel.Message = "Server error."
			respondWithError(w, http.StatusInternalServerError, errorModel)
			return
		}

		err = db.QueryRow("insert into profile (user_id , first_name, last_name,email,phone,job_id,company_id,user_role,image_url) values ($1,$2,$3,$4,$5,$6,$7,$8,' ') returning user_id;",
			DataBean.UserID, SignUpRequest.FirstName, SignUpRequest.LastName,
			SignUpRequest.Email, SignUpRequest.Phone, SignUpRequest.JobID,
			SignUpRequest.CompanyID, SignUpRequest.Role).Scan(&DataBean.UserID)
		logFatal(err)

		row, err := db.Query("insert into position (user_id,latitude,longitude,duration,listener) values ($1,0.5,0.5,6000,false) returning user_id;", DataBean.UserID)

		defer row.Close()
		logFatal(err)

		errorModel.Code = 200
		errorModel.Message = "SignUp Success"
		errorModel.Status = true

		DataBean.Message = "SignUp Success"
		SignUpResponse.Data = DataBean
		SignUpResponse.Meta = errorModel

		responseJSON(w, SignUpResponse)
	}
}

//AddUser to add user to database
func (c Controllers) AddUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var user models.User
		var errorModel models.ErrorModel

		json.NewDecoder(r.Body).Decode(&user)
		if user.Username == "" || user.Password == "" {
			if user.Username == "" {
				errorModel.Message = "Email is missing"
				respondWithError(w, http.StatusBadRequest, errorModel)
				return
			}
			if user.Password == "" {
				errorModel.Message = "Password is missing"
				respondWithError(w, http.StatusBadRequest, errorModel)
				return
			}
		} else {
			hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
			user.Password = string(hash)
			logFatal(err)

			err = db.QueryRow("insert into users (username , password) values ($1,$2) RETURNING user_id,username,password;",
				user.Username, user.Password).Scan(&user.ID, &user.Username, &user.Password)

			if err != nil {
				errorModel.Message = "Server error."
				respondWithError(w, http.StatusInternalServerError, errorModel)
				return
			}
			user.Password = ""
			responseJSON(w, user)

		}
	}
}
