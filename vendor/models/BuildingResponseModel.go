package models

type BuildingResponse struct {
	DataBean BuildingData `json:"data"`
	MetaBean ErrorModel   `json:"meta"`
}

type BuildingData struct {
	Buildings []Building `json:"buildings"`
}

type Building struct {
	BuildingID   int       `json:"building_id"`
	BuildingName string    `json:"building_name"`
	Latlongs     []Latlong `json:"latlong"`
}

type Latlong struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type BuildingDatabaseModel struct {
	BuildingID   int
	BuildingName string
	Latitude     float64
	Longitude    float64
}

type AddBuildingRequest struct {
	CompanyID    int       `json:"company_id"`
	BuildingName string    `json:"building_name"`
	Latlong      []Latlong `json:"latlong"`
}

type AddBuildingResponse struct {
	DataBean AddBuildingData `json:"data"`
	MetaBean ErrorModel      `json:"meta"`
}

type AddBuildingData struct {
	BuildingID   int       `json:"building_id"`
	BuildingName string    `json:"building_name"`
	Latlong      []Latlong `json:"latlong"`
}
