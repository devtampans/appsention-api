package models

//ClockingRequest is a model to store clockin request from employee
type ClockingRequest struct {
	UserID     int     `json:"user_id"`
	CompanyID  int     `json:"company_id"`
	BuildingID int     `json:"building_id"`
	Note       string  `json:"note"`
	Date       string  `json:"date"` // MM-dd-yyyy
	Time       string  `json:"time"`
	Status     string  `json:"status"`
	Location   Latlong `json:"location"`
	Listener   bool    `json:"listener"`
}

type UpdatePositionRequest struct {
	UserID   int     `json:"user_id"`
	Location Latlong `json:"location"`
}

type GetPositionRequest struct {
	UserID    int `json:"user_id"`
	CompanyID int `json:"company_id"`
}

type GetPositionResponse struct {
	DataPosition DataPositionResponse `json:"data"`
	ErrorModel   ErrorModel           `json:"meta"`
}

type DataPositionResponse struct {
	CompanyName string         `json:"company_name"`
	CompanyID   int            `json:"company_id"`
	Employee    []EmployeeData `json:"employees"`
}

type EmployeeData struct {
	UserID    int     `json:"user_id"`
	FirstName string  `json:"first_name"`
	LastName  string  `json:"last_name"`
	Position  Latlong `json:"position"`
}
type UpdatePositionResponse struct {
	DataUpdate DataUpdateResponse `json:"data"`
	ErrorModel ErrorModel         `json:"meta"`
}

type DataUpdateResponse struct {
	Message      string `json:"message"`
	TimeInMillis int    `json:"update_time"`
	Listener     bool   `json:"listener"`
}
