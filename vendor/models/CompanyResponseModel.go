package models

//CompanyResponse the object store company response
type CompanyResponse struct {
	Data DataCompanies `json:"data"`
	Meta ErrorModel    `json:"meta"`
}

//Company as the object to store a company data
type Company struct {
	ID          int    `json:"company_id"`
	CompanyName string `json:"company_name"`
}

//DataCompanies as the object to store slice of comapny
type DataCompanies struct {
	Companies []Company `json:"company"`
}
