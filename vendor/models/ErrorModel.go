package models

//ErrorModel to give response the status to client
type ErrorModel struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Status  bool   `json:"status"`
}
