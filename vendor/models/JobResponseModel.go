package models

type JobResponse struct {
	Data DataJob    `json:"data"`
	Meta ErrorModel `json:"meta"`
}

type JobModel struct {
	ID      int    `json:"id"`
	JobName string `json:"job_name"`
}

type DataJob struct {
	Jobs []JobModel `json:"jobs"`
}
