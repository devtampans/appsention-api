package models

type SignUpRequest struct {
	Username  string `json:"username"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	HireDate  string `json:"hire_date"`
	JobID     int    `json:"job_id"`
	CompanyID int    `json:"company_id"`
	Role      string `json:"role"`
}

type SignUpResponse struct {
	Data DataSignup `json:"data"`
	Meta ErrorModel `json:"meta"`
}

type DataSignup struct {
	UserID    int    `json:"id"`
	AuthToken string `json:"auth_token"`
	Message   string `json:"message"`
}
