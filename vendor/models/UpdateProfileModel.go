package models

//ProfileResponse to store profile datas
type ProfileResponse struct {
	Data DataProfileUser `json:"data"`
	Meta ErrorModel      `json:"meta"`
}

//DataProfileUser Object to store user data
type DataProfileUser struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Image     string `json:"image_url"`
}
